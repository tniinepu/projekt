package pokkerSlot;

import java.util.Arrays;

import lib.TextIO;
import pokkerSlot.Kaart.Mast;

/**
 * 
 * Selle klassi eesmärgiks on simuleerida üks mäng
 * video pokkerit.
 *
 */
public class Mäng {
	
	public static void main(String[] args) {
//	public int Pokker() {
		
		KaardiPakk kaardiPakk;
		Käsi käsi;
		
		kaardiPakk = new KaardiPakk();
		käsi = new Käsi();
		
		kaardiPakk.segamine();
		
		int loend = 0;
		
		do  {
		käsi.lisaKaart(kaardiPakk.jagaKaart());
		loend++;
		} while (loend < 5);

		System.out.println("Su kaardid on: ");
		for (int i = 0; i < käsi.käeSuurus(); i++) {
			System.out.println(" (" + (i+1) + ") " + käsi.kaardiAsukoht(i).KaardiMast() + 
				" " + käsi.kaardiAsukoht(i).KaardiVäärtus());
		}
		System.out.println("Sisesta kaartide numbrid, mida soovid vahetada");
		int arv = TextIO.getlnInt();
		int count = 0;
		while (count < arv) {			
			käsi.eemaldaKaart2(TextIO.getlnInt() - 1);
			count++;			
		}
//		String sisestus = TextIO.getlnString();
//		int sisestus2[] = new int[10];
//		for (int i = 0; i < sisestus.length(); i++) {
//			char temp = sisestus.charAt(i);
//			int temp2 = Character.getNumericValue(temp);
//			System.out.println(temp);
//			sisestus2[i] = temp2;
//				käsi.eemaldaKaart(käsi.kaardiAsukoht(temp2 - 1));
//				käsi.eemaldaKaart2(sisestus2[i] - 1);
//				käsi.lisaKaart(kaardiPakk.jagaKaart());
//		}
	
		loend = käsi.käeSuurus();
		
		while (loend < 5)  {
		käsi.lisaKaart(kaardiPakk.jagaKaart());
		loend++;
		} 
		
		System.out.println("Su uus käsi on: ");
		for (int i = 0; i < käsi.käeSuurus(); i++) {
			System.out.println("     " + käsi.kaardiAsukoht(i).KaardiMast() + 
				" " + käsi.kaardiAsukoht(i).KaardiVäärtus());
		}
		
	}
}

