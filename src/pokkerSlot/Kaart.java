package pokkerSlot;

import java.util.HashMap;
import java.util.Map;

/**
 * Objekt tüübiga Kaart esindab mängu kaarti tavalisest
 * kaardipakist. Kaardil on mast ja väärtus.
 *
 */
public class Kaart {
	
	/**
	 * Teeb kaardi, millel on kindel mast ja väärtus.
	 * @param väärtus2 1-13.
	 * @param mast2 mast.
	 */
	public Kaart(int väärtus2, int mast2) { 
		
		if (väärtus2 > 1 && väärtus2 <= 10) {
			väärtus = Integer.toString(väärtus2);
		}	else if (väärtus2 > 14) { 
			väärtus = "";			
		}	else {
			väärtus = fromInt2(väärtus2).name() ; 
		}
		this.mast = fromInt(mast2); 
		}
	
	/**
	 * Võimaldab enumist kaardi kirjutatud väärtuse
	 * eraldada.
	 */
	private String väärtus;
	public String KaardiVäärtus(){
		return väärtus;
	}
	
	/**
	 * Võimaldab enumist kaardi kirjutatud masti
	 * eraldada.
	 */
	private Mast mast;
	public Mast KaardiMast(){
		return mast;
	}
	
	/**
	 * Järgmised kaks meetodit võimaldavad siduda masti
	 * enumi numbri sõnaga, et seda saaks kasutada.
	 */
	private static final Map<Integer, Mast> intToTypeMap = new HashMap<Integer, Mast>();
	static {
		for (Mast type : Mast.values()) {
			intToTypeMap.put(type.mast, type);
		}
	}
	public static Mast fromInt(int i) {
		Mast type = intToTypeMap.get(Integer.valueOf(i));
		return type;
	}
	
	/**
	 * Mastid.
	 */
	public enum Mast {
	POTI (0),
	ÄRTU (1),
	RUUTU (2),
	RISTI (3),
	JOKKER (4);
	
		private int mast;
		private Mast(int mast) {
			this.mast = mast;
		}
	}
	
	/**
	 * Järgmised kaks meetodit võimaldavad siduda väärtuse
	 * enumi numbri sõnaga, et seda saaks kasutada.
	 */
	private static final Map<Integer, Väärtus> intToTypeMap2 = new HashMap<Integer, Väärtus>();
	static {
		for (Väärtus type : Väärtus.values()) {
			intToTypeMap2.put(type.väärtus, type);
		}
	}
	public static Väärtus fromInt2(int i) {
		Väärtus type = intToTypeMap2.get(Integer.valueOf(i));
		return type;
	}
	
	/**
	 * Väärtused.
	 */
	public enum Väärtus {
		JOKKER (0),
		ÄSS (1),
		POISS (11),
		EMAND (12),
		KUNINGAS (13);
		
			private int väärtus;
			private Väärtus(int väärtus) {
				this.väärtus = väärtus;
			}
	}
}
