package pokkerSlot;

/**
 * Objekt Käsi esindab mängija kaardikätt. Käsi on tühi
 * kui see algselt tehakse ja lisada saab ükskõik kui
 * palju kaarte. Võtsin malli selle klassi jaoks ühest
 * java õpiku ülesandest.
 */
import java.util.ArrayList;

public class Käsi {
	
	private ArrayList<Kaart> käsi;
	
	/**
	 * Teeb käe, mis algselt on tühi.
	 */
	public Käsi() {
		käsi = new ArrayList<Kaart>();
	}	
	
	/**
	 * Eemaldab kõik kaardid käest.
	 */
	public void clear() {
		käsi.clear();
	}	
	
	/**
	 * Lisab kaardi kätte. See läheb lõppu.
	 * @param k kaart.
	 */
	public void lisaKaart(Kaart k) {
		käsi.add(k);
	}
	
	/**
	 * Eemaldab kaardi käest.
	 * @param k kaart.
	 */
	public void eemaldaKaart(Kaart k) {
		käsi.remove(k);
	}
	
	/**
	 * Eemaldab kindal positsioonil kaardi käest.
	 * @param koht positsioon, kust kaart eemaldatakse.
	 */
	public void eemaldaKaart2(int koht) {
		käsi.remove(koht);
	}
	
	/**
	 * Tagastab kaartide arvu käes.
	 */
	public int käeSuurus() {
		return käsi.size();
	}
	
	/**
	 * Tagastab kindlal positsioonil oleva kaardi väärtuse.
	 * @param koht koht, kust tagastatakse kaardi väärtus.
	 */
	public Kaart kaardiAsukoht(int koht) {
		return käsi.get(koht);
	}
}
