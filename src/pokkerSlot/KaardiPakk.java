package pokkerSlot;

/**
 * Objekt KaardiPakk esindab mängukaartide pakki.
 * See on tavaline 52-kaardiga kaardipakk. Võtsin 
 * malli selle klassi tegemiselt ühest java õpiku
 * ülesandest.
 */
public class KaardiPakk {
	
	/**
	 * 52 kaardiga array.
	 */
	private Kaart[] kaardiPakk;
	
	/**
	 * Peab meeles palju kaarte on pakist välja jagatud.
	 */
	private int kaarteKasutatud;
	
	/**
	 * Valmistab mängukaartidest kaardipaki. Pakis on
	 * 52 kaarti. Algselt on kõik järjekorras.
	 */
	public KaardiPakk() {
		kaardiPakk = new Kaart[54];
		int mitmes = 0;
		for (int mast = 0; mast <= 3; mast++) {
			for (int väärtus = 1; väärtus <= 13; väärtus++) {
				kaardiPakk[mitmes] = new Kaart(väärtus,mast);
				mitmes++;
			}
		}
		kaardiPakk[52] = new Kaart(98,4);
		kaardiPakk[53] = new Kaart(99,4);
		
		kaarteKasutatud = 0;
	}

	/**
	 * Tagastab kõik kasutatud kaardid pakki (kui vaja)
	 * ja segab kaardid ära pannes nad suvalisse järjekorda.
	 */
	public void segamine () {
		for(int i = kaardiPakk.length-1; i > 0; i--) {
			int suvaline = (int)(Math.random()*(i+1));
			Kaart temp = kaardiPakk[i];
			kaardiPakk[i] = kaardiPakk[suvaline];
			kaardiPakk[suvaline] = temp;
		}
		kaarteKasutatud = 0;
	}

	/**
	 * Tagastab väärtuse palju kaarte pakis alles on.
	 */
	public int kaarteAlles() {
		return kaardiPakk.length + kaarteKasutatud;
	}
	
	/**
	 * Eemaldab järgmise kaardi pakist ja tagastab selle.
	 */
	public Kaart jagaKaart() {
		kaarteKasutatud++;
		return kaardiPakk[kaarteKasutatud - 1];
	}
	
}
