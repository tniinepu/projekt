package videoPoker;

import java.util.ArrayList;

/**
 * Esindab mängija kätt. Koosneb kaartidest, mida mängija hoiab.
 */
public class Käsi {
	
	/**
	 * Kaartidest koosnev ArrayList ehk siis käsi.
	 */
	private ArrayList<Kaart> käsi;
	
	public Käsi() {
		käsi = new ArrayList<Kaart>();
	}	
	public void clear() {
		käsi.clear();
	}	
	public void lisaKaart(Kaart k) {
		käsi.add(k);
	}

	public void lisaKaart(Kaart k, int koht) {
		käsi.add(koht,k);
	}
	public void eemaldaKaart(Kaart k) {
		käsi.remove(k);
	}
	public void eemaldaKaart(int koht) {
		käsi.remove(koht);
	}
	public int käeSuurus() {
		return käsi.size();
	}
	public Kaart kaardiAsukoht(int koht) {
		return käsi.get(koht);
	}
}
