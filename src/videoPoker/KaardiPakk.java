package videoPoker;

import java.util.ArrayList;
import java.util.Collections;

import videoPoker.Kaart;

/**
 * Objekt Kaardipakk esindab tavalist 52-kaardiga
 * kaardipakki ja lisaks kahte jokkerit.
 */
public class KaardiPakk {
	
	/**
	 * 54 kaardiga arraylist.
	 */
	public ArrayList<Kaart> kaardiPakk;
	
	/**
	 * Peab meeles palju kaarte on pakist välja jagatud.
	 */
	private int kaarteKasutatud;
	
	/**
	 * Paneb kokku tavakaartidest ja jokkeritest kaardipaki ja segab selle.
	 */
	public KaardiPakk() {
		this.kaardiPakk = AnnaKaardipakk();
		this.kaardiPakk.addAll(AnnaJokkerid(2));
		Collections.shuffle(kaardiPakk);

		kaarteKasutatud = 0;
	}
	
	/**
	 * Valmistab 52-kaardilise kaardipaki.
	 * @return 52 kaarti.
	 */
	private ArrayList<Kaart> AnnaKaardipakk(){
		ArrayList<Kaart> kaardiPakk = new ArrayList<Kaart>();
		for (int i = 0; i < 13; i++) {
			Väärtus kaardiVäärtus = Väärtus.values()[i];
			for (int j = 0; j < 4; j++){
				Mast kaardiMast = Mast.values()[j];
				Kaart kaart = new Kaart(kaardiVäärtus, kaardiMast);
				kaardiPakk.add(kaart);
			}
		}
		return kaardiPakk;
	}
	
	/**
	 * Teeb jokkeri.
	 * @param Mitu tükki soovitakse jokkereid.
	 * @return Tagastab soovitud arvu jokkereid.
	 */
	private ArrayList<Kaart> AnnaJokkerid(int arv){
		
		ArrayList<Kaart> jokkerid = new ArrayList<Kaart>();
		for (int i = 0; i < arv; i++) {
			jokkerid.add(new Kaart(Väärtus.Jokker,Mast.Jokker));
		}
		return jokkerid;
	}
	
	/**
	 * Eemaldab kaardipakist järgmise kaardi ja tagastab selle.
	 * @return
	 */
	public Kaart jagaKaart() {
		return kaardiPakk.get(kaarteKasutatud++);
	}

}