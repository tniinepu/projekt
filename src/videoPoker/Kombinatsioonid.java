package videoPoker;

import java.util.ArrayList;

public class Kombinatsioonid {
	
	public int  Kombinatsioonid1() {
		int ühesugusedKaardid = 1, ühesugusedKaardid2 = 1;
		int väikeGrupp = 0, suurGrupp = 0;
		
		for (int i = 13; i >= 1; i--) {
			if (Mäng.AnnaLõppKäsi() > ühesugusedKaardid) {
				if (ühesugusedKaardid != 1) {
					ühesugusedKaardid2 = ühesugusedKaardid;
					väikeGrupp = suurGrupp;
				}
				ühesugusedKaardid = väärtus[i];
				suurGrupp = i;
			}
			else if (väärtus[i] > ühesugusedKaardid2) {
				ühesugusedKaardid2 = väärtus[i];
				väikeGrupp = i;
			}
		}
		boolean mast = true;
		for (int i = 0; i < 4; i++) {
			if (Kaart[i].annaMast() != Kaart[i+1].annaMast()) {
				mast = false;
			}
		}
		int kõrgeimReaVäärtus = 0;
		boolean rida = false;
		for (int i = 1; i <= 9; i++) {
			if (väärtus[i] == 1 && väärtus[i+1] == 1 && väärtus[i+2] == 1 &&
				väärtus[i+3] == 1 && väärtus[i+4] == 1) {
					rida = true;
					kõrgeimReaVäärtus = i+4;
					break;
			}
		}
		if (väärtus[10] == 1 && väärtus[11] == 1 && väärtus[12] == 1 && 
			väärtus[13] == 1 && väärtus[1] == 1) {
			rida = true;
			kõrgeimReaVäärtus = 14;
		}
		//Käe Hindamine
		
		//Kuninglik mastirida
		if (rida && mast && kõrgeimReaVäärtus == 14) {
			return 250;
		}
		//Mastirida
		if (rida && mast) {
			return 50;
		}
		//Nelik
		if (ühesugusedKaardid == 4) {
			return 25;
		}
		//Maja
		if (ühesugusedKaardid == 3 && ühesugusedKaardid2 == 2) {
			return 9;
		}
		//Mast
		if (mast) {
			return 6;
		}
		//Rida
		if (rida) {
			return 4;
		}
		//Kolmik
		if (ühesugusedKaardid == 3 && ühesugusedKaardid2 != 2) {
			return 3;
		}
		//Kaks paari
		if (ühesugusedKaardid == 2 && ühesugusedKaardid2 == 2) {
			return 2;
		}
		//Üks paar
		if (ühesugusedKaardid == 2 && ühesugusedKaardid2 == 1) {
			if (poiss või kõrgem) {
					return 1;
			}
			else return 0;
		}
		//Ei ole midagi
		if (ühesugusedKaardid == 1) {
			return 0;
		}
				
	}

}
