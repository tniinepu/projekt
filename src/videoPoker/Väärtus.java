package videoPoker;

/**
 * Esindab kaartide vääruseid (1-14).
 */
public enum Väärtus {
	Äss(1),
	Kaks(2),
	Kolm(3),
	Neli(4),
	Viis(5),
	Kuus(6),
	Seitse(7),
	Kaheksa(8),
	Üheksa(9),
	Kümme(10),
	Poiss(11),
	Emand(12),
	Kuningas(13),
	Jokker(14);
	
	private Väärtus(int väärtus) {
		
	}
	
}

