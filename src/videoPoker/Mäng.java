package videoPoker;

import java.util.ArrayList;

import lib.TextIO;

/**
 * Objekti Mäng eesmärk on simuleerida video pokkeri mänge. Jagab mängijale kaardid,
 * annab võimaluse neid vahetada ja lõpuks hindab lõppkäe kombinatsiooni väärtust.
 */
public class Mäng {
	
	public static void main(String[] args) {
//	public int Mäng1() {
		
		KaardiPakk kaardiPakk = new KaardiPakk();
		Käsi käsi = new Käsi();
		
		int loend = 0;
		
		do  {
			käsi.lisaKaart(kaardiPakk.jagaKaart());
			loend++;
		} while (loend < 5);

		System.out.println("Su kaardid on: ");
		for (int i = 0; i < käsi.käeSuurus(); i++) {
			System.out.println(" (" + (i+1) + ") " + käsi.kaardiAsukoht(i).annaMast() + 
				" " + käsi.kaardiAsukoht(i).annaVäärtus());
		}
		
		ArrayList<Integer> vahetatavadKaardid = KüsiKasutajaltVahetavadKaardid();

		for (int i = 0; i < vahetatavadKaardid.size(); i++) {
			int vahetatavaKaardiAsukoht = vahetatavadKaardid.get(i);
			käsi.eemaldaKaart(vahetatavaKaardiAsukoht);
			käsi.lisaKaart(kaardiPakk.jagaKaart(),vahetatavaKaardiAsukoht);
			
		}
		
		System.out.println("Su uus käsi on: ");
		for (int i = 0; i < käsi.käeSuurus(); i++) {
			System.out.println("     " + käsi.kaardiAsukoht(i).annaMast() + 
				" " + käsi.kaardiAsukoht(i).annaVäärtus());
		}
		
//		ArrayList<Integer> LõppKäsi = new ArrayList<Integer>(); {
//			for (int i = 0; i < käsi.käeSuurus(); i++) {
//				käsi.kaardiAsukoht(i).annaMast();
//				käsi.kaardiAsukoht(i).annaVäärtus();
//			}
//		}
//		
//		for (int i = 0; i < LõppKäsi.size(); i++) {
//			
//		}
		
	}
			
	public static ArrayList<Integer> KüsiKasutajaltVahetavadKaardid() { //static maha pärast
		ArrayList<Integer> vahetatavadKaardid = new ArrayList<Integer>();
		while(true){

			System.out.println("Sisesta kaartide asukoha numbrid, mida soovid vahetada");
			try {
				int sisestatudArvud = TextIO.getlnInt();
				if (sisestatudArvud>54321) throw new Exception();
					
				String sisestatudString = Integer.toString(sisestatudArvud);
				for (int i = 0; i < sisestatudString.length(); i++) {
					char temp = sisestatudString.charAt(i);
					int vahetatavKaart = Character.getNumericValue(temp)-1;
					if (vahetatavadKaardid.contains(vahetatavKaart)) {
						vahetatavadKaardid.clear();
						throw new Exception();
					}
					vahetatavadKaardid.add(vahetatavKaart);
				}
				break;
			} 
			catch(Exception e) {
				System.out.println("Vigane sisend!");
			}
		}
		return vahetatavadKaardid;
		
		
	}
	
//	public static ArrayList<Integer> AnnaLõppKäsi() {
//		ArrayList<Integer> LõppKäsi = AnnaLõppKäsi();
//			return LõppKäsi;
//		} 
	
	
}

