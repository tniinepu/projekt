package videoPoker;

/**
 * Objekt Kaart esindab tavalist mängu kaarti, millel on mast ja väärtus.
 */
public class Kaart {
	
	
	private Mast mast;
	private Väärtus väärtus;
	
	public Kaart (Väärtus väärtus, Mast mast) {
		this.väärtus = väärtus;
		this.mast = mast;
	}
	
	public Mast annaMast() {
		return mast;
	}
	
	public void paneMast(Mast mast) {
		this.mast = mast;
	}
	
	public Väärtus annaVäärtus() {
		return väärtus;
	}
	
	public void paneVäärtus(Väärtus väärtus) {
		this.väärtus = väärtus;
	}

	
}